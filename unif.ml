open Expr
open Type
open Exceptions 
type subst = (expr * types) list
           
let compteur_type = ref 0
let new_type () = incr compteur_type; X(!compteur_type)

let premiere_occ x j =
  let rec aux u j =
    match j with
    | (Var(v), t) :: q when v = u -> t
    | _ :: q -> aux u q
    | [] -> new_type ()
  in
  match x with
  | Var(u) -> aux u j
  | _ -> failwith "n'arrive pas"

let rec all_assoc x t j =
  match x with
  | Var(u) ->
    (match j with
    | (Var(v), t1) :: q when u = v -> (t, t1) :: (all_assoc (Var(u)) t q)
    | (LetIn(Var(v), _, _), _) :: _ when u = v -> []
    | _ :: q -> all_assoc (Var(u)) t q
    | [] -> []
    )
  | Couple(y, z) ->
    let t1 = new_type () in
    let t2 = new_type () in
    (t, Etoile(t1, t2)) :: (all_assoc y t1 j) @ (all_assoc z t2 j)
  | Cons(y, z) ->
    let t1 = new_type () in
    let t2 = new_type () in
    (t1, t2) :: (t, List(t1)) :: (all_assoc y t1 j) @ (all_assoc z t2 j)
  | _ -> []

let rec trouvepb e t expr_to_type =
  match e with
  | Val(VInt(_)) ->
    [(t, Int)], (e, t) :: expr_to_type
  | OpArithm(e1, e2, _) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb e1 Int expr_to_type in
    let u2, j2 = trouvepb e2 Int expr_to_type in
    (t, Int) :: (t1, Int) :: (t2, Int) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type
  
  | OpComp(e1, e2, Eg) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb e1 t1 expr_to_type in
    let u2, j2 = trouvepb e2 t2 expr_to_type in
    (t1, t2) :: (t, Bool) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type
  | OpComp(e1, e2, _)  ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb e1 Int expr_to_type in
    let u2, j2 = trouvepb e2 Int expr_to_type in
    (t1, Int) :: (t2, Int) :: (t, Bool) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type
  
  | Val(VBool(_)) ->
    [(t, Bool)], (e, t) :: expr_to_type
  | Not(e1) ->
    let t1 = new_type () in
    let u1, j1 = trouvepb e1 t1 expr_to_type in
    (t, Bool) :: (t1, Bool) :: u1, (e, t) :: j1
  | OpBool(e1, e2, _) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb e1 Bool expr_to_type in
    let u2, j2 = trouvepb e2 Bool expr_to_type in
    (t, Bool) :: (t1, Bool) :: (t2, Bool) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type
  | Ite(e1, e2, e3) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let t3 = new_type () in
    let u1, j1 = trouvepb e1 t1 expr_to_type in
    let u2, j2 = trouvepb e2 t2 expr_to_type in
    let u3, j3 = trouvepb e3 t3 expr_to_type in
    (t, t2) :: (t1, Bool) :: (t2, t3) :: u1 @ u2 @ u3, (e, t) :: j1 @ j2 @ j3 @ expr_to_type

  | Apply(e1, e2) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let t3 = new_type () in
    let u1, j1 = trouvepb e1 t1 expr_to_type in
    let u2, j2 = trouvepb e2 t2 expr_to_type in
    (t, t3) :: (t1, Fleche(t2, t3)) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type
  | Val(VFun(e1, e2, _)) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb e1 t1 expr_to_type in
    let u2, j2 = trouvepb e2 t2 expr_to_type in
    let u3 = all_assoc e1 t1 j2 in
    (t, Fleche(t1, t2)) :: u1 @ u2 @ u3, (e, t) :: j1 @ j2 @ expr_to_type
  
  | LetIn(e1, e2, e3) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let t3 = new_type () in
    let u1, j1 = trouvepb e1 t1 expr_to_type in
    let u2, j2 = trouvepb e2 t2 expr_to_type in
    let u3, j3 = trouvepb e3 t3 expr_to_type in
    let u4 = all_assoc e1 t1 j3 in
    (t1, t2) :: (t, t3) :: u1 @ u2 @ u3 @ u4, (e, t) :: j1 @ j2 @ j3 @ expr_to_type
  | Var(s) ->
    let t1 = new_type () in
    (t1, t) :: [], (e, t) :: expr_to_type
  
  | Couple(e1, e2) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb e1 t1 expr_to_type in
    let u2, j2 = trouvepb e2 t2 expr_to_type in
    (t, Etoile(t1, t2)) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type
  | Val(VCouple(v1, v2)) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb (Val(v1)) t1 expr_to_type in
    let u2, j2 = trouvepb (Val(v2)) t2 expr_to_type in
    (t, Etoile(t1, t2)) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type
  
  | PrInt(e1) ->
    let u1, j1 = trouvepb e1 t expr_to_type in
    (t, Int) :: u1, (e, t) :: j1
  
  | EmptyList ->
    let t1 = new_type () in
    [(t, List(t1))], [(e, t)]
  | Cons(h, q) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb h t1 expr_to_type in
    let u2, j2 = trouvepb q t2 expr_to_type in
    (t, List(t1)) :: (List(t1), t2) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type
  | Val(VEmptyList) ->
    let t1 = new_type () in
    [(t, List(t1))], [(e, t)]
  | Val(VCons(h, q)) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let u1, j1 = trouvepb (Val(h)) t1 expr_to_type in
    let u2, j2 = trouvepb (Val(q)) t2 expr_to_type in
    (t, List(t1)) :: (List(t1), t2) :: u1 @ u2, (e, t) :: j1 @ j2 @ expr_to_type

  | MatchWith(e1, EndCase) ->
    [], []
  | MatchWith(e1, Case(e2, e3, sc)) ->
    let t1 = new_type () in
    let t2 = new_type () in
    let t3 = new_type () in
    let t4 = new_type () in
    let u1, j1 = trouvepb e1 t1 expr_to_type in
    let u2, j2 = trouvepb e2 t2 expr_to_type in
    let u3, j3 = trouvepb e3 t3 expr_to_type in
    let u4 = all_assoc e2 t2 j3 in
    let u5, j5 = trouvepb (MatchWith(e1, sc)) t4 expr_to_type in

    (t1, t2) :: (t, t3) :: (t3, t4) :: u1 @ u2 @ u3 @ u4 @ u5, (e, t) :: j1 @ j2 @ j3 @ j5 @ expr_to_type