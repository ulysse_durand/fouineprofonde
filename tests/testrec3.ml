let rec f x = (if x < 2 then 1 else x * g (x - 1)) and g x = (if x < 2 then 1 else x * f (x - 1)) in f

