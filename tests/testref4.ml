let y = ref 0 in
let inc y =
  y := !y+1
in
inc y;
prInt(!y)