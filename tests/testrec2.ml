let rec modulo a b = if a < b then a else modulo (a-b) b in let rec pgcd a b = if b = 0 then a else pgcd b (modulo a b) in pgcd 18 93
