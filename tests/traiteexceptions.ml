open Exceptions


let traitevaleur x = match x with
  | Val y -> y
  | Exc n -> raise E n 