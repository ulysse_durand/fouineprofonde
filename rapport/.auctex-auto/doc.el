(TeX-add-style-hook
 "doc"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "a4paper" "12pt")))
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("inputenc" "utf8") ("changebar" "xcolor" "leftbars")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "inputenc"
    "amssymb"
    "amsmath"
    "xcolor"
    "enumitem"
    "bbold"
    "changebar"
    "listings"
    "hyperref")
   (TeX-add-symbols
    '("norm" 1)
    "letitle"
    "leauthor")
   (LaTeX-add-environments
    "myindentpar"
    "answer")
   (LaTeX-add-bibliographies
    "bib")
   (LaTeX-add-lengths
    "mydepth"
    "myheight")
   (LaTeX-add-saveboxes
    "mybox"))
 :latex)

