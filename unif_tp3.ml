(* Dans ce fichier, on définit les types qui vont nous servir pour écrire des termes *)
open Type
(* Types des variables *)
type var = int

(* Types des opérateurs/fonctions dans la signature des termes *)
type symbol = string

(* Type des termes : Un terme est soit une variables, soit une fonction (opérateur) appliqué à une liste d'arguments *)
type t = 
  | Var of var
  | Op of symbol * t list

type problem = 
  (t * t) list

let rec transformetype t =
	match t with
	| Int -> Op("Int",[])
	| Bool -> Op("Bool", [])
	| X k -> Var k
	| Fleche(x, y) -> Op("Fleche", [transformetype x; transformetype y])
	| Etoile(x, y) -> Op("Etoile", [transformetype x; transformetype y])
	| List(x) -> Op("Liste", [transformetype x])

let rec transformeunif u =
	match u with
	| Var k -> X k 
	| Op("Int", _) -> Int
	| Op("Bool", _) -> Bool
	| Op("Fleche", [a; b]) -> Fleche(transformeunif a, transformeunif b)
	| Op("Etoile", [a; b]) -> Etoile(transformeunif a, transformeunif b)
	| Op("Liste", [a]) -> List(transformeunif a)

type subst = (var * t) list
           
exception Not_unifyable (*à "raise" si le problème n'a pas de solution*)

(*Renvoie true si x apparaît dans term*)
let rec appear (x : var) (term : t) : bool =
  match term with
		| Var(v) -> x = v
		| Op(_, tlist) -> appear_list x tlist
and appear_list x tlist =
	match tlist with
		| h :: q -> (appear x h) || (appear_list x q)
		| [] -> false
;;
      
(*Effectue la substitution sigma(term) = term[new_x/x] *)
let rec replace ((x, new_x) : var * t) (term : t) : t =
	match term with
		| Var(v) when x = v-> new_x
		| Var(v) -> Var(v)
		| Op(f, tlist) -> Op(f, replace_list x new_x tlist)
and replace_list x new_x tlist =
	match tlist with
		| h :: q -> (replace (x, new_x) h) :: (replace_list x new_x q)
		| [] -> []
;;

(*Implémente l'unification de deux termes*)
let rec replace_prob x new_x prob =
	match prob with
		| (tg, td) :: qprob -> (replace (x, new_x) tg, replace (x, new_x) td) :: (replace_prob x new_x qprob)
		| [] -> []
;;

let rec destruct_op opg opd prob =
	match opg, opd with
		| hg :: qg, hd :: qd -> (hg, hd) :: (destruct_op qg qd prob)
		| [], [] -> prob
    | _, _ -> failwith "n'arrive pas"
;;

let rec unify (pb : problem) : subst =
	(* reprendre la solution en argument d'une fonction auxiliaire pour mettre à jour 
		 les var dans la solution *)
	match pb with
		| [] -> []
		| (tg, td) :: qprob ->
			begin
				match tg, td with
				| Var(vg), Var(vd) when vg = vd -> unify qprob
				| _, Var(vd) when appear vd tg -> raise Not_unifyable
				| _, Var(vd) -> (vd, tg) :: (unify (replace_prob vd tg qprob))
				| Var(vg), _ -> unify((td, Var(vg)) :: qprob)
				| Op(fg, tglist), Op(fd, tdlist) when fd = fg ->
					unify (destruct_op tglist tdlist qprob)
				| Op(fg, tglist), Op(fd, tdlist) -> raise Not_unifyable
			end
;;
